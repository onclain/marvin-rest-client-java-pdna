package com.adidas.dam.marvin.client.query.filter;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.ProductImageAsset;

public class NotFilterTest {

	private NotFilter notFilter;
	private AssetFilter filter1 = new AssetFilter(ProductImageAsset.ARTICLE_NAME, Operand.EQUAL, "Superstar");
	private static final String FILTER_STR1 = "(" + ProductImageAsset.ARTICLE_NAME + " eq 'Superstar')";
	
	@Test
	public void testCompile() {
		notFilter = new NotFilter(filter1);
		assertThat(notFilter.compile(), is("(Not " + FILTER_STR1 + ")"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPassNullToConstructor() {
		notFilter = new NotFilter(null);
	}

}
