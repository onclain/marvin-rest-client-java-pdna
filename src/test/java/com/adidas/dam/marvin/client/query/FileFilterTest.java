package com.adidas.dam.marvin.client.query;

import static org.junit.Assert.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Before;
import org.junit.Test;

public class FileFilterTest {
	
	private FileFilter filter;

	@Before
	public void setUp() throws Exception {
		filter = FileFilter.create();
	}

	@Test
	public void testEmptyFilter() {
		assertEquals("", filter.buildQueryString());
	}
	
	@Test
	public void testFormatDefinition() {
		assertEquals("?format=eps", filter.withFormat(FileFormat.EPS).buildQueryString());
	}
	
	@Test
	public void testWatermarks() {
		assertEquals("?watermark=true", filter.enableWatermark().buildQueryString());
		assertEquals("?watermark=false", filter.disableWatermark().buildQueryString());
	}

	@Test 
	public void testWidthValues() {
		
		// check width values
		assertEquals("?width=100", filter.withWidth(100).buildQueryString());
		assertEquals("?width=200", filter.withWidth(200).buildQueryString());
		
		// check invalid values
		int[] testVals = { 0, 10001, -1, (Integer.MAX_VALUE + 1) };
		for (int testVal : testVals) {
			try {
				filter.withWidth(testVal).buildQueryString();
				fail("Setting width to " + testVal
						+ " should throw an exception.");
			} catch (IllegalArgumentException iae) {
			}
		}
	}
	
	@Test
	public void testMaxWidthValues() {
		// check maxWidth
		assertEquals("?maxWidth=500", filter.withMaxWidth(500).buildQueryString());
		assertEquals("?maxWidth=999", filter.withMaxWidth(999).buildQueryString());
		
		// check invalid values
		int[] testVals = { 0, 10001, -1, (Integer.MAX_VALUE + 1) };
		for (int testVal : testVals) {
			try {
				filter.withMaxWidth(testVal).buildQueryString();
				fail("Setting maxWidth to " + testVal
						+ " should throw an exception.");
			} catch (IllegalArgumentException iae) {
			}
		}
	}
	
	@Test
	public void testCombinedWidthValues() {
		
		// check simple values
		assertThat(filter.withWidth(100).withMaxWidth(200).buildQueryString(), 
				allOf(containsString("width=100"), containsString("maxWidth=200")));
		assertThat(filter.withWidth(888).withMaxWidth(999).buildQueryString(),
				allOf(containsString("width=888"), containsString("maxWidth=999")));
		
		// check exceptional flows
		try {
			filter.withWidth(0).withMaxWidth(100).buildQueryString();
			fail("Setting width to 0 should throw an exception.");
		} catch(IllegalArgumentException iae) {
		}

		try {
			filter.withWidth(100).withMaxWidth(10001).buildQueryString();
			fail("Settings maxWidth beyond the boundary should throw an exception.");
		} catch(IllegalArgumentException iae) {}

		try {
			filter.withWidth(2000).withMaxWidth(1000).buildQueryString();
			fail("Setting width to a higer value than maxWidth should throw an exception.");
		} catch(IllegalArgumentException iae) {}
	}

	@Test 
	public void testHeightValues() {
		
		// check width values
		assertEquals("?height=100", filter.withHeight(100).buildQueryString());
		assertEquals("?height=200", filter.withHeight(200).buildQueryString());
		
		// check invalid values
		int[] testVals = { 0, 10001, -1, (Integer.MAX_VALUE + 1) };
		for (int testVal : testVals) {
			try {
				filter.withHeight(testVal).buildQueryString();
				fail("Setting height to " + testVal
						+ " should throw an exception.");
			} catch (IllegalArgumentException iae) {
			}
		}
	}

	@Test
	public void testMaxHeightValues() {
		// check maxWidth
		assertEquals("?maxHeight=500", filter.withMaxHeight(500).buildQueryString());
		assertEquals("?maxHeight=999", filter.withMaxHeight(999).buildQueryString());
		
		// check invalid values
		int[] testVals = { 0, 10001, -1, (Integer.MAX_VALUE + 1) };
		for (int testVal : testVals) {
			try {
				filter.withMaxHeight(testVal).buildQueryString();
				fail("Setting maxHeight to " + testVal
						+ " should throw an exception.");
			} catch (IllegalArgumentException iae) {
			}
		}
	}
	
	@Test
	public void testCombinedHeightValues() {
		// check simple values
		assertThat(filter.withHeight(100).withMaxHeight(200).buildQueryString(), 
				allOf(containsString("height=100"), containsString("maxHeight=200")));
		assertThat(filter.withHeight(888).withMaxHeight(999).buildQueryString(),
				allOf(containsString("height=888"), containsString("maxHeight=999")));
		
		// check exceptional flows
		try {
			filter.withHeight(0).withMaxHeight(100).buildQueryString();
			fail("Setting height to 0 should throw an exception.");
		} catch(IllegalArgumentException iae) {
		}

		try {
			filter.withHeight(100).withMaxHeight(10001).buildQueryString();
			fail("Settings maxHeight beyond the boundary should throw an exception.");
		} catch(IllegalArgumentException iae) {}

		try {
			filter.withHeight(2000).withMaxHeight(1000).buildQueryString();
			fail("Setting height to a higer value than maxHeight should throw an exception.");
		} catch(IllegalArgumentException iae) {}	}
	
	@Test
	public void testUnit() {
		assertEquals("?units=pixel", filter.withUnit(Unit.PIXEL).buildQueryString());
		assertEquals("?units=percent", filter.withUnit(Unit.PERCENT).buildQueryString());
	}
	
	@Test
	public void testResolution() {
		int[] validVals = {18, 300, 1000, 10000};
		for (int validVal : validVals) {
			assertThat(filter.withResolution(validVal).buildQueryString(), containsString("resolution="+validVal));
		}
		
		int[] testVals =  {17, 10001, -1, (Integer.MAX_VALUE + 1)};
		for (int testVal : testVals) {
			try {
				filter.withResolution(testVal).buildQueryString();
				fail("Setting resolution to " + testVal + " should throw an exception");
			} catch(IllegalArgumentException iae) {}
		}
	}
	
	@Test
	public void testQuality() {
		int[] validVals = {1, 25, 50, 75, 100};
		int[] invalidVals = {0, 101, -1, Integer.MAX_VALUE};
		
		for (int validVal : validVals) {
			assertThat(filter.withQuality(validVal).buildQueryString(), containsString("quality=" + validVal));
		}
		
		for (int invalidVal : invalidVals) {
			try {
				filter.withQuality(invalidVal).buildQueryString();
				fail("Setting quality to " + invalidVal + " should throw an exception");
			} catch(IllegalArgumentException iae) {
				
			}
		}
	}
	
	@Test
	public void testPadding() {
		assertThat(filter.buildQueryString(), not(containsString("padding")));
		assertThat(filter.withPadding(0, 0, 0, 0).buildQueryString(), containsString("padding=0,0,0,0"));
		assertThat(filter.withPadding(11, 22, 33, 44).buildQueryString(), containsString("padding=11,22,33,44"));
		assertThat(filter.withPadding().buildQueryString(), containsString("padding=0,0,0,0"));
		assertThat(filter.withPadding(10).buildQueryString(), containsString("padding=10,0,0,0"));
		assertThat(filter.withPadding(10, 20).buildQueryString(), containsString("padding=10,20,0,0"));
		assertThat(filter.withPadding(10, 20, 30).buildQueryString(), containsString("padding=10,20,30,0"));
		assertThat(filter.withPadding(10, 20, 30, 40).buildQueryString(), containsString("padding=10,20,30,40"));
		assertThat(filter.withPadding(10, 20, 30, 40, 50).buildQueryString(), containsString("padding=10,20,30,40"));
		
		try {
			filter.withPadding(-1, 0, 0, 0).buildQueryString();
			fail("Passing any negative value as padding should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.withPadding(0, -1, 0, 0).buildQueryString();
			fail("Passing any negative value as padding should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}
		
		try {
			filter.withPadding(0, 0, -1, 0).buildQueryString();
			fail("Passing any negative value as padding should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.withPadding(0, 0, 0, -1).buildQueryString();
			fail("Passing any negative value as padding should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}
		
		try {
			filter.withPadding(10001, 0, 0, 0).buildQueryString();
			fail("Passing any padding bigger than MAX_WIDTH or MAX_HEIGHT should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.withPadding(0, 10001, 0, 0).buildQueryString();
			fail("Passing any padding bigger than MAX_WIDTH or MAX_HEIGHT should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.withPadding(0, 0, 10001, 0).buildQueryString();
			fail("Passing any padding bigger than MAX_WIDTH or MAX_HEIGHT should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.withPadding(0, 0, 0, 10001).buildQueryString();
			fail("Passing any padding bigger than MAX_WIDTH or MAX_HEIGHT should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.withPadding(5000, 0, 5001, 0).buildQueryString();
			fail("Passing top and bottom padding values which in sum are bigger than MAX_HEIGHT should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}		
		
		try {
			filter.withPadding(0, 5000, 0, 5001).buildQueryString();
			fail("Passing top and bottom padding values which in sum are bigger than MAX_HEIGHT should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}		
	}
	
	@Test
	public void testBackgroundColor() {
		assertThat(filter.buildQueryString(), not(containsString("background")));
		
		try {
			filter.withBackgroundColor(new RGBColor()).buildQueryString();
			fail("Specifying a background color without enabling clipping or padding should throw an exception");
		} catch (IllegalArgumentException iae) {
		}
		
		assertThat(filter.enableClipping().withBackgroundColor(new RGBColor())
				.buildQueryString(), containsString("background=FFFFFF"));
		
		assertThat(filter.enableClipping().withBackgroundColor(new RGBColor(false))
				.buildQueryString(), containsString("background=FFFFFF"));
		
		assertThat(filter.enableClipping().withBackgroundColor(new RGBColor(true))
				.buildQueryString(), containsString("background=Transparent"));
		
		assertThat(filter.enableClipping().withBackgroundColor(new RGBColor(15, 15, 15))
				.buildQueryString(), containsString("background=0F0F0F"));
		
		try {
			filter.enableClipping().withBackgroundColor(new RGBColor(-1, 15, 15))
			.buildQueryString();
			fail("Passing a value outside the boundaries of [0:255] to one of the color channels should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.enableClipping().withBackgroundColor(new RGBColor(256, 15, 15))
			.buildQueryString();
			fail("Passing a value outside the boundaries of [0:255] to one of the color channels should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.enableClipping().withBackgroundColor(new RGBColor(15, -1, 15))
			.buildQueryString();
			fail("Passing a value outside the boundaries of [0:255] to one of the color channels should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.enableClipping().withBackgroundColor(new RGBColor(15, 256, 15))
			.buildQueryString();
			fail("Passing a value outside the boundaries of [0:255] to one of the color channels should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.enableClipping().withBackgroundColor(new RGBColor(15, 15, -1))
			.buildQueryString();
			fail("Passing a value outside the boundaries of [0:255] to one of the color channels should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}

		try {
			filter.enableClipping().withBackgroundColor(new RGBColor(15, 15, 256))
			.buildQueryString();
			fail("Passing a value outside the boundaries of [0:255] to one of the color channels should throw an exception.");
		} catch (IllegalArgumentException iae) {
		}
	}

	@Test
	public void testAlignment() {
		assertThat(filter.buildQueryString(), not(containsString("alignment")));
		assertThat(filter.withAlignment(null).buildQueryString(), not(containsString("alignment")));
		assertThat(filter.withAlignment(Direction.TOP_LEFT).buildQueryString(), containsString("alignment=TopLeft"));
		assertThat(filter.withAlignment(Direction.TOP_CENTER).buildQueryString(), containsString("alignment=TopCenter"));
		assertThat(filter.withAlignment(Direction.TOP_RIGHT).buildQueryString(), containsString("alignment=TopRight"));
		assertThat(filter.withAlignment(Direction.CENTER_LEFT).buildQueryString(), containsString("alignment=CenterLeft"));
		assertThat(filter.withAlignment(Direction.CENTER_CENTER).buildQueryString(), containsString("alignment=CenterCenter"));
		assertThat(filter.withAlignment(Direction.CENTER_RIGHT).buildQueryString(), containsString("alignment=CenterRight"));
		assertThat(filter.withAlignment(Direction.BOTTOM_LEFT).buildQueryString(), containsString("alignment=BottomLeft"));
		assertThat(filter.withAlignment(Direction.BOTTOM_CENTER).buildQueryString(), containsString("alignment=BottomCenter"));
		assertThat(filter.withAlignment(Direction.BOTTOM_RIGHT).buildQueryString(), containsString("alignment=BottomRight"));
	}
	
	
	@Test
	public void testClipping() {
		assertThat(filter.buildQueryString(), not(containsString("clippingPath")));
		assertThat(filter.enableClipping().buildQueryString(), containsString("clippingPath=true"));
		assertThat(filter.disableClipping().buildQueryString(), containsString("clippingPath=false"));
	}
	
	@Test
	public void testFrameExtraction() {
		int[] validVals = {1, 10, 100, 1000, Integer.MAX_VALUE};
		int[] invalidVals = {0, -1, (Integer.MAX_VALUE + 1)};
		
		for (int validVal : validVals) {
			assertThat(filter.extractFrame(validVal).buildQueryString(), containsString("frameIndex=" + validVal));
		}
		
		for (int invalidVal : invalidVals) {
			try {
				filter.extractFrame(invalidVal).buildQueryString();
				fail("Trying to extract a frame of " + invalidVal + " should throw an exception");
			} catch(IllegalArgumentException iae) {
				
			}
		}
	}
	
	@Test
	public void testColorProfile() {
		assertThat(filter.withColorProfile(ColorProfile.ADOBE_RGB_1998).buildQueryString(), 
				containsString("colorProfile=AdobeRGB1998"));
		assertThat(filter.withColorProfile(ColorProfile.COATED_GRACOL_2006).buildQueryString(), 
				containsString("colorProfile=CoatedGRACol2006"));
		assertThat(filter.withColorProfile(null).buildQueryString(), not(containsString("colorProfile")));
	}

	@Test
	public void testEpsMode() {
		
		try {
			filter.withEpsMode(EpsMode.PLAIN).buildQueryString();
			fail("Setting EpsMode without setting FileFormat to EPS should throw an exception.");
		} catch (IllegalArgumentException iae) {}
		
		assertThat(filter
				.withFormat(FileFormat.EPS)
				.withEpsMode(EpsMode.PLAIN)
				.buildQueryString(), 
				containsString("epsMode=plain"));
		
		assertThat(filter
				.withFormat(FileFormat.EPS)
				.withEpsMode(EpsMode.WITH_PREVIEW)
				.buildQueryString(), 
				containsString("epsMode=withpreview"));
		
		assertThat(filter
				.withFormat(FileFormat.EPS)
				.withEpsMode(null)
				.buildQueryString(), 
				not(containsString("epsMode")));
		
	}
	
}
