package com.adidas.dam.marvin.client.query.filter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

import org.junit.Test;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.Article;

public class OrFilterTest {

	private OrFilter orFilter;
	private ArticleFilter filter1 = new ArticleFilter(Article.ARTICLE_NAME, Operand.EQUAL, "Superstar");
	private static final String FILTER_STR1 = "(" + Article.ARTICLE_NAME + " eq 'Superstar')";
	private ArticleFilter filter2 = new ArticleFilter(Article.CONF_EXPIRY_DATE, Operand.GREATER_THAN, "2014-10-01");
	private static final String FILTER_STR2 = "(" + Article.CONF_EXPIRY_DATE + " gt DateTime'2014-10-01')";
	private ArticleFilter filter3 = new ArticleFilter(Article.CONFIDENTIAL, Operand.EQUAL, "true");
	private static final String FILTER_STR3 = "(" + Article.CONFIDENTIAL + " eq true)";
	
	@Test
	public void testCompile() {
		orFilter = new OrFilter(filter1, filter2, filter3);
		assertThat(orFilter.compile(), allOf(
				containsString(FILTER_STR1),
				containsString(FILTER_STR2),
				containsString(FILTER_STR3)
				));
		assertThat(orFilter.compile().split("or").length, is(3));
	}
	
	@Test
	public void testSingleFilter() {
		orFilter = new OrFilter(filter1);
		assertThat(orFilter.compile(), allOf(
				containsString(FILTER_STR1),
				not(containsString("and"))
				));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyFilter() {
		orFilter = new OrFilter();
		orFilter.compile();
	}

}
