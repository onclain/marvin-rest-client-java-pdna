package com.adidas.dam.marvin.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetsResponse {
	
	@JsonProperty("Count")
	private long count;
	
	@JsonProperty("Results")
	private List<ProductImageAsset> assets;

	/**
	 * @return the count
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @return the assets
	 */
	public List<ProductImageAsset> getAssets() {
		return assets;
	}
		

}
