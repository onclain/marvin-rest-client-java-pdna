package com.adidas.dam.marvin.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import com.adidas.dam.marvin.domain.Filterable;
import com.adidas.dam.marvin.domain.ODataType;
import com.adidas.dam.marvin.domain.Orderable;
import com.adidas.dam.marvin.domain.Selectable;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class AnnotationHelper {
	
	private AnnotationHelper() {
		super(); 
	}

	public static boolean hasFilterableAnnotation(final Class<?> clazz, final String fieldName) {
		return fieldHasAnnotation(findField(clazz, fieldName), Filterable.class);
	}
	
	public static boolean hasOrderableAnnotation(final Class<?> clazz, final String fieldName) {
		return fieldHasAnnotation(findField(clazz, fieldName), Orderable.class);
	}
	
	public static boolean hasSelectableAnnotation(final Class<?> clazz, final String fieldName) {
		return fieldHasAnnotation(findField(clazz, fieldName), Selectable.class);
	}
	
	public static String getODataType(final Class<?> clazz, final String fieldName) {
		final Field field = findField(clazz, fieldName);
		if (field != null) {
			final ODataType oDataType = field.getAnnotation(ODataType.class);
			if (oDataType != null) {
				return oDataType.value();
			}
		}
		return "string";
	}
	
	private static boolean fieldHasAnnotation(final Field field, 
			final Class<? extends Annotation> annotationClass) {
		return ((field != null) && (field.getAnnotation(annotationClass) != null));
	}
	
	private static Field findField(final Class<?> clazz, final String fieldName) {
		Class<?> myClass = clazz;
		do {
			for (Field field : myClass.getDeclaredFields()) {
				final JsonProperty prop = field.getAnnotation(JsonProperty.class);
				if ((prop != null) && prop.value().equals(fieldName)) {
					return field;
				}
			}
			myClass = myClass.getSuperclass();
		} while (myClass != null);
		return null;
	}
}
