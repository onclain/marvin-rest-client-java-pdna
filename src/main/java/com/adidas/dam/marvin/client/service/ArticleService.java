package com.adidas.dam.marvin.client.service;

import java.util.List;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.domain.Article;

/**
 * Implementing classes are used to retrieve {@link Article} associated
 * information from Marvin.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public interface ArticleService {

	/**
	 * Retrieves a {@link List} of {@link Article} objects from Marvin that
	 * match the given {@link Query} criteria. 
	 * 
	 * @param query	{@link Query} to be used to retrieve {@link Article}s
	 * @return	{@link List} of {@link Article}s on success – may be empty if no 
	 * 		{@link Article} matches.
	 * @throws MarvinClientException On connection or retrieval issues.
	 */
	List<Article> find(Query query) throws MarvinClientException;

	/**
	 * Counts number of {@link Article}s matching the given {@link Query} criteria.
	 * Please note that this does not consider paging but will always return the number
	 * or overall matching elements.
	 * 
	 * @param query	{@link Query} to be used to count {@link Article}s
	 * @return Number of {@link Article}s matching.
	 * @throws MarvinClientException On connection or retrieval issues.
	 */
	long count(Query query) throws MarvinClientException;
	
}
