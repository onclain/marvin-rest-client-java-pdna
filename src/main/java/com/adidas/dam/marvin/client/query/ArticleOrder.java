package com.adidas.dam.marvin.client.query;

import com.adidas.dam.marvin.domain.Article;

/**
 * Concrete and {@link Article} type safe implementation of an Order Expression
 * which can be used to order {@link MarvinClient#getArticles} results.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class ArticleOrder extends AbstractOrderExpression {

	/**
	 * Default constructor expects the Field on which to order as well as a
	 * direction defined by an instance of {@link Order} enum. E.g.:
	 * 
	 * <pre>
	 * {@code
	 * Query query = QueryBuilder
	 *    .create()
	 *    .withOrder(new ArticleOrder(Article.ARTICLE_NAME, Order.ASC))
	 *    .build();
	 * }
	 * </pre>
	 * 
	 * @param orderField Name of the field on which to order. 
	 * @param order Direction of ordering defined by either {@link Order.ASC} or {@link Order.DESC}
	 */
	public ArticleOrder(String orderField,
			Order order) {
		super(Article.class, orderField, order);
	}


}
