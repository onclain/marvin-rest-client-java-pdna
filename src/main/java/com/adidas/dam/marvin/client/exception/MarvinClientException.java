package com.adidas.dam.marvin.client.exception;

/**
 * Exception class which wrappes all exceptions that could occur
 * during execution of different {@link MarvinClient} operations.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class MarvinClientException extends Exception {

	private static final long serialVersionUID = 1887797783878052750L;

	/**
	 * Constructor that is only setting the error message by delegating 
	 * it to its super-class.
	 * 
	 * @param message	The exception message to be set.
	 */
	public MarvinClientException(String message) {
		super(message);
	}
	
	/**
	 * Constructor receiving a message and a throwable delegating to 
	 * super constructor.
	 * 
	 * @param message
	 * @param t
	 */
	public MarvinClientException(String message, Throwable t) {
		super(message, t);
	}
	
	/**
	 * Constructor that is only receiving a {@link Throwable} and 
	 * delegating to super constructor.
	 * 
	 * @param t
	 */
	public MarvinClientException(Throwable t) {
		super(t);
	}
}
