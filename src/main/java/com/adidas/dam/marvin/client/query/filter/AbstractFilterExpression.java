package com.adidas.dam.marvin.client.query.filter;

import org.springframework.util.StringUtils;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.Entity;
import com.adidas.dam.marvin.util.AnnotationHelper;

public abstract class AbstractFilterExpression implements Filter {

	private static final int MAX_VALUE_LENGTH = 2048;
	public static final String FILTER_PREFIX = "$filter";
	
	private final Class<? extends Entity> clazz;
	private String field;
	private Operand operand;
	private String value;
	
	public AbstractFilterExpression(final Class<? extends Entity> clazz, 
			final String field, final Operand operand, final String value) {
		this.clazz = clazz;
		setField(field);
		setOperand(operand);
		setValue(value);
	}
	
	private void setField(final String field) {
		if (field == null) {
			throw new IllegalArgumentException("Entity field must not be null.");
		}
		if (!AnnotationHelper.hasFilterableAnnotation(clazz, field)) {
			throw new IllegalArgumentException(field + " is not valid for filtering.");
		}
		this.field = field;
	}
	
	private void setOperand(final Operand operand) {
		if (operand == null) {
			throw new IllegalArgumentException("Operand must not be null.");
		}
		this.operand = operand;
	}
	
	private void setValue(final String value) {
		if (!StringUtils.hasLength(value)) {
			throw new IllegalArgumentException("Value must not be empty.");
		}
		if (value.length() > MAX_VALUE_LENGTH) {
			throw new IllegalArgumentException("Value must not be longer than " 
					+ MAX_VALUE_LENGTH + " chars.");
		}
		this.value = value;
	}
	
	private String getFormattedValue() {
		final String type = AnnotationHelper.getODataType(clazz, field);
		String formattedValue = value;
		if ("string".equalsIgnoreCase(type)) {
			formattedValue = "'" + value + "'";
		} else if ("datetime".equalsIgnoreCase(type)) {
			formattedValue = "DateTime'" + value + "'";
		}
		return formattedValue;
	}

	public String compile() {
		return "(" + field + " " + 
				operand + " " +
				getFormattedValue() + ")";
	}

}
