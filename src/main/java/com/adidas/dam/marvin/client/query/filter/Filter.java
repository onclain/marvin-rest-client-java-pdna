package com.adidas.dam.marvin.client.query.filter;


public interface Filter {
		
	String compile();
		
}
