package com.adidas.dam.marvin.client.query;

/**
 * Defines valid modes for eps image retrievals. 
 *  
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum EpsMode {
	PLAIN("plain"), 
	WITH_PREVIEW("withpreview"),
	;
	
	private String outputString;
	
	private EpsMode(String outputString) {
		this.outputString = outputString;
	}
	
	/**
	 * Returns String represantation of the instance which can directly
	 * be used as query param.
	 */
	public String toString() {
		return outputString;
	}
}
