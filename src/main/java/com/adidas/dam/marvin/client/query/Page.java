package com.adidas.dam.marvin.client.query;

import java.util.HashMap;
import java.util.Map;

import com.adidas.dam.marvin.domain.ProductImageAsset;

/**
 * Used to define pagination used while retrieving {@link Article}s or 
 * {@link ProductImageAsset}s since the REST services only allow a limited ammount 
 * of results per page.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class Page {
	
	private static final String SKIP_PREFIX = "$skip";
	private static final String TOP_PREFIX = "$top";
	
	private static final long MAX_TOP_VALUE = 50;
	
	private long skip;
	private long top;

	public Page(long skip, long top) {
		setSkip(skip);
		setTop(top);
	}

	/**
	 * Sets the number of values to be skipped which equals<br/>
	 * <br/>
	 * (&lt;page number&gt; - 1) * &lt;page size&gt;
	 * 
	 * @param skip the number of results to be skipped.
	 */
	public void setSkip(long skip) {
		if (skip < 0) {
			throw new IllegalArgumentException("Skip value must be greater ot equal zero.");
		}

		this.skip = skip;
	}

	/**
	 * Sets the number of elements to retrieve which equals the page size.
	 * 
	 * @param top the number of results to be retrieved per call.
	 */
	public void setTop(long top) {
		if (top <= 0 || top > MAX_TOP_VALUE) {
			throw new IllegalArgumentException("Top value must be within boundaries. "
					+"0 < top <= " + MAX_TOP_VALUE + ". But is " + top);
		}

		this.top = top;
	}

	/**
	 * Used to build the query string which will be appended on the REST clients
	 * GET-call. The {@link Map} contains the url param name as keys and the values
	 * set through {@link setSkip} and {@link setTop} as values.
	 * 
	 * @return a Map containing the query param names as key associated to the set values.
	 */
	public Map<String, String> build() {
		final Map<String, String> params = new HashMap<String, String>();
		params.put(SKIP_PREFIX, Long.toString(skip));
		params.put(TOP_PREFIX, Long.toString(top));
		return params;
	}
	
}
