package com.adidas.dam.marvin.client.query.filter;

public class NotFilter implements Filter {

	private final Filter filter;
	
	public NotFilter(final Filter filter) {
		if (filter == null) {
			throw new IllegalArgumentException("Passed filter expression must not be null.");
		}
		
		this.filter = filter;
	}
	
	@Override
	public String compile() {
		return "(Not " + filter.compile() + ")";
	}

}
