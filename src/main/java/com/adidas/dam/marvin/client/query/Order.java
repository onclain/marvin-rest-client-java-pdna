package com.adidas.dam.marvin.client.query;

/**
 * Definition of Order direction. Either ascending or descending.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum Order {
	ASC, DESC
}
