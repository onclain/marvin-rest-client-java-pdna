package com.adidas.dam.marvin.client.service;

import java.util.List;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.domain.ProductImageAsset;

public interface AssetService {

	List<ProductImageAsset> find(Query query) throws MarvinClientException;
	
	long count(Query query) throws MarvinClientException;
	
}
