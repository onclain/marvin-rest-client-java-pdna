package com.adidas.dam.marvin.client.query;

import java.util.HashSet;
import java.util.Set;

import com.adidas.dam.marvin.client.query.filter.Filter;

/**
 * Helps creating {@link Query} objects that can be passed to {@link MarvinClient}
 * service calls like {@link MarvinClient.getArticles} or 
 * {@link MarvinClient.countAssets}. 
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public class QueryBuilder {

	private final Format format = Format.JSON;
	private Page page;
	private AbstractOrderExpression order;
	private Set<Filter> filters;
	
	/**
	 * Static factory method returning a new instance of a {@link QueryBuilder}
	 * which allows easy chaining of calls.
	 * 
	 * @return a new instance of {@link QueryBuilder}
	 */
	public static QueryBuilder create() {
		return new QueryBuilder();
	}
	
	/**
	 * Adds paging according to the passed values.
	 * 
	 * @param skip the number of results to be skipped.
	 * @param top the number of results to be returned per call.
	 * @return the reference to a {@link QueryBuilder} allowing chaining.
	 */
	public QueryBuilder withPaging(final long skip, final long top) {
		return withPaging(new Page(skip, top));
	}
	
	/**
	 * Adds paging according to the passed values.
	 * 
	 * @param page the paging to be used to retrieve results.
	 * @return the reference to a {@link QueryBuilder} allowing chaining.
	 */
	public QueryBuilder withPaging(final Page page) {
		this.page = page;
		return this;
	}
	
	/**
	 * Adds order information to the {@link Query}.
	 * 
	 * @param order the order expression containing a field and a direction.
	 * @return the reference to a {@link QueryBuilder} allowing chaining.
	 */
	public QueryBuilder withOrder(final AbstractOrderExpression order) {
		this.order = order;
		return this;
	}
	
	/**
	 * Adds a Filter to this query. Multiple calls to this method
	 * will combine those filters using an or expression.
	 * 
	 * @param filter the Filter to be added to the query.
	 * @return the reference to a {@link QueryBuilder} allowing chaining.
	 */
	public QueryBuilder withFilter(final Filter filter) {
		if (filters == null) {
			filters = new HashSet<Filter>();
		}
		filters.add(filter);
		return this;
	}
	
	/**
	 * Constructs a new {@link Query} object using the given values.
	 * 
	 * @return a new Query object containing the passed values.
	 */
	public Query build() {
		return new Query(
				format,
				page,
				order,
				filters
				);
	}
}
