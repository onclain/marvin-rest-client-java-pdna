package com.adidas.dam.marvin.client.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.domain.AssetsResponse;
import com.adidas.dam.marvin.domain.ProductImageAsset;

@Service
public class AssetServiceImpl implements AssetService {

	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(AssetServiceImpl.class);
	
	private static final String ASSET_SERVICE_PATH = "/contents/productimages/assets";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${baseUrl}")
	private String baseUrl;
	
	@Override
	public List<ProductImageAsset> find(Query query) throws MarvinClientException {		
		return fireQuery(query).getAssets();
	}

	@Override
	public long count(Query query) throws MarvinClientException {
		return fireQuery(query).getCount();
	}
	
	private AssetsResponse fireQuery(Query query) throws MarvinClientException {
		try {
		return restTemplate.getForObject(
				baseUrl + ASSET_SERVICE_PATH + query.buildParamsString(), 
				AssetsResponse.class);
		} catch (Exception e) {
			throw new MarvinClientException("Error while sending request.", e);
		}
	}

}
