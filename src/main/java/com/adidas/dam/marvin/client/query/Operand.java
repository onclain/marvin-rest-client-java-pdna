package com.adidas.dam.marvin.client.query;

import com.adidas.dam.marvin.client.query.filter.AbstractFilterExpression;

/**
 * Defines all allowed operations that may be used in a {@link AbstractFilterExpression}.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum Operand {
	EQUAL("eq"),
	NOT_EQUAL("ne"),
	GREATER_THAN("gt"),
	GREATER_OR_EQUAL("ge"),
	LESS_THAN("lt"),
	LESS_OR_EQUAL("le");
	
	private String abbrv;
	
	private Operand(String abbrv) {
		this.abbrv = abbrv;
	}

	/**
	 * Returns string representation of the chosen value which can directly be used to build 
	 * a query string.
	 */
	public String toString() {
		return abbrv;
	}
}
