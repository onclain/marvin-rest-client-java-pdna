package com.adidas.dam.marvin.client.query;

import java.util.HashMap;
import java.util.Map;

/**
 * Allowed formats used to talk to Marvin REST services. Defaults to json since 
 * its performance is usually slightly better due to removed overhead. 
 * Should not be overwritten by the user since currently only a JacksonMapper 
 * is configured to unmarshall responses.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public enum Format {
	XML("xml"), JSON("json");
	
	private static final String FORMAT_PREFIX = "$format";
	
	private String formatString;
	
	private Format(String formatString) {
		this.formatString = formatString;
	}
	
	/**
	 * Returns a map including url param variable names as well as value.
	 * 
	 * @return map of format definitions (url param name as key, value as value)
	 */
	public Map<String, String> build() {
		final Map<String, String> param = new HashMap<String, String>();
		param.put(FORMAT_PREFIX, formatString);
		return param;
	}
}
