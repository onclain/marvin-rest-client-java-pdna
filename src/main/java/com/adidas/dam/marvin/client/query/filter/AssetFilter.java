package com.adidas.dam.marvin.client.query.filter;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.ProductImageAsset;

public class AssetFilter extends AbstractFilterExpression {

	public AssetFilter(String field, Operand operand,
			String value) {
		super(ProductImageAsset.class, field, operand, value);
	}

}
