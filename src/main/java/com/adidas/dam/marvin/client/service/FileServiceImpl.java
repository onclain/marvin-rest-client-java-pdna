package com.adidas.dam.marvin.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.FileFilter;

@Service
public class FileServiceImpl implements FileService {
	
	private static final String FILES_SERVICE_PATH = "/files/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${baseUrl}")
	private String baseUrl;

	@Override
	public byte[] getFile(FileFilter filter, String fileId) throws RestClientException, MarvinClientException  {
		return restTemplate.getForObject(
				baseUrl + FILES_SERVICE_PATH + fileId + filter.buildQueryString(), 
				byte[].class);
	}
}
