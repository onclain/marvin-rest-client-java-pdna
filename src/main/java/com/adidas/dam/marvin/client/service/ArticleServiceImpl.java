package com.adidas.dam.marvin.client.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.Query;
import com.adidas.dam.marvin.domain.Article;
import com.adidas.dam.marvin.domain.ArticleResponse;

/**
 * Simple Implementation of {@link ArticleService} making use of Spring's 
 * {@link RestTemplate} and it's auto configured JacksonMapper.
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
@Service
public class ArticleServiceImpl implements ArticleService {

	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(ArticleServiceImpl.class);
	
	private static final String ARTICLE_SERVICE_PATH = "/contents/productimages/articles/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${baseUrl}")
	private String baseUrl;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Article> find(Query query) throws MarvinClientException {
		return fireQuery(query).getArticles();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long count(Query query) throws MarvinClientException {
		return fireQuery(query).getCount();
	}
	
	/**
	 * Fires passed {@link Query} against the article service URL. Calls 
	 * {@link Query#builParamsString} to get the query string which has to be appended 
	 * to the service's base url.
	 * 
	 * @param query	{@link Query} to be used to call the service
	 * @return Automatically Jackson un-wrapped {@link ArticleResponse} which gets further 
	 * 		used by {@link find} and {@link count}.
	 * @throws MarvinClientException On URL, connection or unmarshalling issues.
	 */
	private ArticleResponse fireQuery(Query query) throws MarvinClientException {
		try {
		return restTemplate.getForObject(
				baseUrl + ARTICLE_SERVICE_PATH + query.buildParamsString(), 
				ArticleResponse.class);
		} catch(Exception e) {
			throw new MarvinClientException("Error while sending request.", e);
		}
	}

}
