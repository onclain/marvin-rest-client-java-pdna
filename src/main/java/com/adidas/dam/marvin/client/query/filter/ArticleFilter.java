package com.adidas.dam.marvin.client.query.filter;

import com.adidas.dam.marvin.client.query.Operand;
import com.adidas.dam.marvin.domain.Article;

public class ArticleFilter extends AbstractFilterExpression {

	public ArticleFilter(String field,
			Operand operand, String value) {
		super(Article.class, field, operand, value);
	}

}
