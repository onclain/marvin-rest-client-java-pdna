package com.adidas.dam.marvin.client.query;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.StringUtils;

/**
 * Holds all possible filter settings that may be passed to a {@link MarvinClient#getImage} 
 * or {@link MarvinClient#getimageAsync} call. Implemented as a Builder which might be 
 * chained with different options, e.g.:
 * 
 * <pre>
 * {@code
 * String queryParams = FileFilter.create()
 *    .withFormat(FileFormat.PNG)
 *    .enableClipping()
 *    .withMaxWidth(500)
 *    .withMaxHeight(500)
 *    .buildQueryString();
 * }
 * </pre>
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 * 
 */
@SuppressWarnings({"TooManyMethods" ,"TooManyFields"})
public class FileFilter {

	private static final int MIN_WIDTH = 1;
	private static final int MAX_WIDTH = 10000;
	private static final int MIN_HEIGHT = 1;
	private static final int MAX_HEIGHT = 10000;
	private static final int MIN_RESOLUTION = 18;
	private static final int MAX_RESOLUTION = 10000;
	private static final int MIN_QUALITY = 1;
	private static final int MAX_QUALITY = 100;
	private static final int MIN_PADDING = 0;
	private static final String[] PADDING_ORDER = {"top", "right", "bottom", "left"};
	private static final int MIN_FRAME = 1;
	private static final String WATERMARK_PREFIX = "watermark";
	private static final String MAX_WIDTH_PREFIX = "maxWidth";
	private static final String MAX_HEIGHT_PREFIX = "maxHeight";
	private static final String WIDTH_PREFIX = "width";
	private static final String HEIGHT_PREFIX = "height";
	private static final String RESOLUTION_PREFIX = "resolution";
	private static final String QUALITY_PREFIX = "quality";
	private static final String PADDING_PREFIX = "padding";
	private static final String ALIGNMENT_PREFIX = "alignment";
	private static final String CLIPPING_PREFIX = "clippingPath";
	private static final String FRAME_INDEX_PREFIX = "frameIndex";
	private static final String EPS_MODE_PREFIX = "epsMode";
	private static final String COLOR_PROFILE_PREFIX = "colorProfile";
	
	private FileFormat format;
	private Boolean watermark;
	private Integer maxWidth;
	private Integer maxHeight;
	private Integer width;
	private Integer height;
	private Unit unit;	
	private Integer resolution;
	private Integer quality;
	private int[] padding; 
	private RGBColor backgroundColor;
	private Direction alignment;
	private Boolean clipping;
	private Integer frameIndex;
	private ColorProfile colorProfile;
	private EpsMode epsMode;
	
	/**
	 * Static generator method that returns a new instance to allow chaining
	 * of different calls.
	 *  
	 * @return A new instance of FileFilter.
	 */
	public static FileFilter create() {
		return new FileFilter();
	}
	
	/**
	 * Sets the desired file format to this Filter. See {@link FileFormat} for
	 * valid format expressions.
	 * 
	 * @param format The desired output file format. 
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withFormat(final FileFormat format) {
		this.format = format;
		return this;
	}
	
	/**
	 * Enables Watermarking
	 * 
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter enableWatermark() {
		watermark = Boolean.TRUE;
		return this;
	}
	
	/**
	 * Disables Watermarking. Make sure that your user and client settings
	 * allow disabling of this option – otherwise you'll encounter exceptions. 
	 * 
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter disableWatermark() {
		watermark = Boolean.FALSE;
		return this;
	}
	
	/**
	 * Sets the desired width of the resulting object.
	 * 
	 * @param width desired width
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withWidth(final int width) {
		validateInteger(width, MIN_WIDTH, MAX_WIDTH, "Width");
		this.width = width;
		return this;
	}
	
	/**
	 * Sets the desired maximum width of the resulting object. If used in 
	 * conjunction with {@link maxHeight} Marvin will keep the aspect ratio 
	 * without cropping the image but resizing to satisfy these values.
	 * 
	 * @param width desired maximum width
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withMaxWidth(final int maxWidth) {
		validateInteger(maxWidth, MIN_WIDTH, MAX_WIDTH, "Maximum width");
		this.maxWidth = maxWidth;
		return this;
	}
	
	/**
	 * Sets the desired height of the resulting object.
	 * 
	 * @param width desired height
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withHeight(final int height) {
		validateInteger(height, MIN_HEIGHT, MAX_HEIGHT, "Height");
		this.height = height;
		return this;
	}
	
	/**
	 * Sets the desired maximum height of the resulting object. If used in 
	 * conjunction with {@link maxWidth} Marvin will keep the aspect ratio 
	 * without cropping the image but resizing to satisfy these values.
	 * 
	 * @param width desired maximum width
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withMaxHeight(final int maxHeight) {
		validateInteger(maxHeight, MIN_HEIGHT, MAX_HEIGHT, "Maximum height");
		this.maxHeight = maxHeight;
		return this;
	}
	
	/**
	 * Sets the unit for different operations to either {@link Unit.PIXEL} or 
	 * {@link Unit.PERCENT}.
	 * 
	 * @param unit Unit to be used for different operations
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withUnit(final Unit unit) {
		this.unit = unit;
		return this;
	}
	
	/**
	 * Sets the desired resolution in pixels per inch.
	 * 
	 * @param resolution desired pixel per inch
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withResolution(final int resolution) {
		validateInteger(resolution, MIN_RESOLUTION, MAX_RESOLUTION, "Resolution");
		this.resolution = resolution;
		return this;
	}
	
	/**
	 * For file formats supporting a quality setting – like JPG – sets 
	 * the desired quality. 
	 * 
	 * @param quality
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withQuality(final int quality) {
		validateInteger(quality, MIN_QUALITY, MAX_QUALITY, "Quality");
		this.quality = quality;
		return this;
	}
	
	/**
	 * Set values to enable padding – canvas size actually bigger than the image itself.
	 * Order of definition is:
	 * <ol>
	 * <li>top</li>
	 * <li>right</li>
	 * <li>bottom</li>
	 * <li>left</li>
	 * </ol>
	 * 
	 * Missing arguments will be filled with zeros, redundant values exceeding four
	 * will be cut of.
	 * 
	 * @param padding values to be used for padding
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withPadding(final int... padding) {
		for(int i = 0; (i < padding.length && i < PADDING_ORDER.length); i++) {
			validateInteger(padding[i], MIN_PADDING, 
					((i % 2 == 0) ? MAX_HEIGHT : MAX_WIDTH), PADDING_ORDER[i]);
		}

		if (padding.length < 4) {
			// be error save so pad the passed one first with zeros, 
			// copy the values and then cut it to desired length
			final int[] fillArr = new int[] {0, 0, 0, 0};
			final int[] tmpArr = Arrays.copyOf(padding, padding.length + fillArr.length);
			System.arraycopy(fillArr, 0, tmpArr, padding.length, fillArr.length);
			this.padding = Arrays.copyOf(tmpArr, 4);
		} else {
			this.padding = Arrays.copyOf(padding, 4);
		}
		
		return this;
	}

	/**
	 * Set values to enable padding – canvas size actually bigger than the image itself.
	 * 
	 * @param top padding on the top border
	 * @param right padding on the right
	 * @param bottom padding on bottom border
	 * @param left padding on the left
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withPadding(final int top, final int right, final int bottom, final int left) {
		return withPadding(new int[] {top, right, bottom, left});
	}
	
	/**
	 * If clipping or padding is enabled sets a background color using a {@link RGBColor} 
	 * object which allows to define a color in RGB color model or 'Transparent'.
	 * 
	 * @param backgroundColor Color to be used as background color.
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withBackgroundColor(final RGBColor backgroundColor) {
		this.backgroundColor = backgroundColor;
		return this;
	}
	
	/**
	 * If canvas is actually bigger than the image the alignment option might be used to
	 * align the image onto the canvas. See {@link Direction} for allowed values.
	 * 
	 * @param alignment	The alignment direction to be used.
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withAlignment(final Direction alignment) {
		this.alignment = alignment;
		return this;
	}
	
	/**
	 * Enables clipping meaning that a images is cut out of the background.
	 * 
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter enableClipping() {
		this.clipping = Boolean.TRUE;
		return this;
	}
	
	/**
	 * Disables clipping meaning that a images are send along with background.
	 * 
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter disableClipping() {
		this.clipping = Boolean.FALSE;
		return this;
	}
	
	/**
	 * Allows extracting of multiframe images – like gifs – to extract a particular 
	 * single frame.
	 * 
	 * @param frameIndex The number of the frame to extract.
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter extractFrame(final int frameIndex) {
		validateInteger(frameIndex, MIN_FRAME, Integer.MAX_VALUE, "Frame index");
		this.frameIndex = frameIndex;
		return this;
	}
	
	/**
	 * Enable a output specific Color profile. Usually screen media should select 
	 * {@link ColorProfile.ADOBE_RGB_1998} whereas print media better works with
	 * {@link ColorProfile.COATED_GRACOL_2006}. If unsure leave untouched.
	 * 
	 * @param colorProfile The color profile to be applied onto the returned image.
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withColorProfile(final ColorProfile colorProfile) {
		this.colorProfile = colorProfile;
		return this;
	}
	
	/**
	 * If {@link FileFormat.EPS} was passed to {@link withFormat} since sets the value
	 * of how a preview image should be applied to the returned eps document. Whereas
	 * {@link EpsMde.PLAIN} omits an embedded preview image, {@link EpsMode.WITH_PREVIEW}
	 * embeds a preview.
	 * 
	 * @param epsMode the desired mode (with or without preview image)
	 * @return reference onto this Filter to allow chaining.
	 */
	public FileFilter withEpsMode(final EpsMode epsMode) {
		this.epsMode = epsMode;
		return this;
	}
	
	/**
	 * Using the given properties builds a query string which might directly be attached
	 * to a URL-call against the file service.
	 * 
	 * @return a fully compiled and ready to use query string.
	 */
	public String buildQueryString() {
		validateParams();
		
		final Set<String> params = new HashSet<>();
		
		if (format != null) {
			params.add(FileFormat.FORMAT_PREFIX + "=" + format);
		}
		
		if (watermark != null) {
			params.add(WATERMARK_PREFIX + "=" + watermark);
		}
		
		if (maxWidth != null) {
			params.add(MAX_WIDTH_PREFIX + "=" + maxWidth);
		}
		
		if (maxHeight != null) {
			params.add(MAX_HEIGHT_PREFIX + "=" + maxHeight);
		}
		
		if (width != null) {
			params.add(WIDTH_PREFIX + "=" + width);
		}
		
		if (height != null) {
			params.add(HEIGHT_PREFIX + "=" + height);
		}
		
		if (unit != null) {
			params.add(Unit.UNIT_PREFIX + "=" + unit);
		}
		
		if (resolution != null) {
			params.add(RESOLUTION_PREFIX + "=" + resolution);
		}
		
		if (quality != null) {
			params.add(QUALITY_PREFIX + "=" + quality);
		}
		
		if (padding != null) {
			params.add(PADDING_PREFIX + "=" + padding[0] + "," + padding[1] 
					+ "," + padding[2] + "," + padding[3]);
		}
		
		if (backgroundColor != null) {
			params.add(backgroundColor.compile());
		}
		
		if (alignment != null) {
			params.add(ALIGNMENT_PREFIX + "=" + alignment);
		}
		
		if (clipping != null) {
			params.add(CLIPPING_PREFIX + "=" + clipping);
		}
		
		if (frameIndex != null) {
			params.add(FRAME_INDEX_PREFIX + "=" + frameIndex);
		}
		
		if (colorProfile != null) {
			params.add(COLOR_PROFILE_PREFIX + "=" + colorProfile);
		}
		
		if (epsMode != null) {
			params.add(EPS_MODE_PREFIX + "=" + epsMode);
		}
		
		return (!params.isEmpty() ? "?" +
				StringUtils.collectionToDelimitedString(params, "&") : "");
	}
	
	/**
	 * Validation method to validate a given integer against lower and upper 
	 * boundaries. 
	 * 
	 * @param value The value to be checked.
	 * @param lowerBoundary The lower boundary (excluding) to be checked against.
	 * @param upperBoundary The upper boundary (excluding) to be checked against.
	 * @param attribute The name of the passed attribute which should be part
	 * 		of the possibly thrown Exception.
	 */
	private void validateInteger(final int value, final int lowerBoundary,
                                 final int upperBoundary, final String attribute) {
		if (value < lowerBoundary || value > upperBoundary) {
			throw new IllegalArgumentException(attribute + " must be within boundaries [" 
					+ lowerBoundary + ":" + upperBoundary + "]. Actual value was: " + value);
		}
	}
	
	/**
	 * Executes some intra-value dependencies as well as brief and simple sanity checks.
	 */
	private void validateParams() {
		if (epsMode != null && (format == null || !format.equals(FileFormat.EPS))) {
			throw new IllegalArgumentException("EPS Mode is " + epsMode 
					+ " but file format is " + format);
		}
		
		if (backgroundColor != null && (clipping == null || !clipping) && padding == null) {
			throw new IllegalArgumentException("If Background color is specified either clipping "
					+ "should be true or padding set.");
		}
		
		if (width != null && maxWidth != null && (width > maxWidth)) {
			throw new IllegalArgumentException("If width and maxWidth are defined width should be "
					+ "less or equal than maxWidth.");
		}
		
		if (height != null && maxHeight != null && (height > maxHeight)) {
			throw new IllegalArgumentException("If height and maxHeight are defined height should "
					+ "be less or equal than maxHeight.");
		}
		
		if (padding != null && padding[0] + padding[2] > MAX_HEIGHT) {
			throw new IllegalArgumentException("Sum of top and bottom padding is bigger than "
					+ "maximum image height.");
		}
		
		if (padding != null && padding[1] + padding[3] > MAX_WIDTH) {
			throw new IllegalArgumentException("Sum of left and right padding is bigger than "
					+ "maximum image width.");
		}
	}
}
