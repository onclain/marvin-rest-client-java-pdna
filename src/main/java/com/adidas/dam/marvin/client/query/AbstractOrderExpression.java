package com.adidas.dam.marvin.client.query;

import java.util.HashMap;
import java.util.Map;

import com.adidas.dam.marvin.domain.Entity;
import com.adidas.dam.marvin.domain.ProductImageAsset;
import com.adidas.dam.marvin.util.AnnotationHelper;

/**
 * Abstract base class to e.g. {@link ArticleOrder} or {@link AssetOrder} 
 * which have been created for type safety reasons.  
 * 
 * @author Daniel Eichten <daniel.eichten@adidas-group.com>
 */
public abstract class AbstractOrderExpression {
	
	private static final String ORDERBY_PREFIX = "$orderby";
	
	private final String orderField;
	private final Order order;
	
	/**
	 * Constructor defining the Entity class which is being dealt with (should be either
	 * {@link Article} or {@link ProductImageAsset}, the field used to be ordered upon and 
	 * an order direction defined by passing an instance of {@link Order}.
	 * 
	 * @param clazz	Class definition for type safety.
	 * @param orderField Field name to be used for ordering.
	 * @param order Order direction.
	 */
	protected AbstractOrderExpression(final Class<? extends Entity> clazz, 
			final String orderField, final Order order) {
		if (orderField == null) {
			throw new IllegalArgumentException("Order field has to be specified.");
		}
		
		if (!AnnotationHelper.hasOrderableAnnotation(clazz, orderField)) {
			throw new IllegalArgumentException(orderField + " is not allowed for ordering.");
		}
		
		if (order == null) {
			throw new IllegalArgumentException("Order direction has to be defined.");
		}
		
		this.orderField = orderField;
		this.order = order;
	}
	
	/**
	 * Builds a {@link Map} of {@link String}s using the query params name as key and 
	 * the order expression as value.
	 * 
	 * @return A {@link Map} containing query param identifiers and order expression which
	 * can be used to compile the correct query string.
	 */
	public Map<String, String> build() {
		final Map<String, String> param = new HashMap<String, String>();
		param.put(ORDERBY_PREFIX, orderField + " " + order.toString().toLowerCase());
		return param;
	}

}
