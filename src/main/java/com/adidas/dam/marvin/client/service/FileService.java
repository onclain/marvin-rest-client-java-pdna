package com.adidas.dam.marvin.client.service;

import com.adidas.dam.marvin.client.exception.MarvinClientException;
import com.adidas.dam.marvin.client.query.FileFilter;

public interface FileService {

	byte[] getFile(FileFilter filter, String fileId) throws MarvinClientException;
		
}
